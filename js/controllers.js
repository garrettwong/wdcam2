'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('MainCtrl', ['$scope', 'notify', function($scope, notify) {
  	$scope.title = "Dashboard";
  	$scope.test = notify.getGetData();

    $scope.requestControl = function() {
      //var pass = prompt()

      var name = prompt("What is your name?");

      if (name === null || name === '') {

      } else {
        // fire notification change event and update in the events controller
        $scope.$broadcast("requestControl", name);        
      }
    };

    $scope.videoMode = function() {
      $scope.$broadcast("videoMode");
    };
  }])
  .controller('ImageCtrl', ['$scope', 'graphService', function($scope, graphService) {
  	$scope.title = "Live Video Feed";

  	$scope.graphData = graphService.getData();
  	$scope.graphActions = graphService.getGraphActions();

    $scope.locations = [{name: 'WD 4th Floor Breakroom'}, {name: 'WD 1st Floor Breakroom'}, {name: 'IRV1-2080'}]

    $scope.changeLocation = function (location) {
      alert('Changing location to ' + location);
    }

    $scope.$on('videoMode', function () {
      $scope.showImage = false;
      $scope.showVideo = true;
    });

    $scope.showVideo = false;
    $scope.showImage = true;

    $scope.speed = 7;
    $scope.setSpeed = function(speed) {
      $scope.speed = speed;
      if (speed === 25) {
        $scope.fast = true;
        $scope.slow = false;
      } else {
        $scope.fast = false;
        $scope.slow = true;
      }
    };

    var moveCamera = function(direction, percent) {
      $.get('/api/' + direction + '/' + percent)
          .done(function() { console.log('Moved ' + direction) })
          .fail(function() { alert('Failed to move ' + direction) });
    };

    $scope.handleClick = function(direction) {
      var percent = $scope.speed;
      moveCamera(direction, percent);
    };

    var readKeys = function () {
        $(document).keydown(function(e) {
          var direction;
          var percent = $scope.speed;
          switch(e.which) {
            case 37: // left
            direction = 4;
            moveCamera(direction, percent);
            break;
            case 38: // up
            direction = 1;
            moveCamera(direction, percent);
            break;
            case 39: // right
            direction = 2;
            moveCamera(direction, percent);
            break;
            case 40: // down
            direction = 3;
            moveCamera(direction, percent);
            break;
          }

          console.log(new Date());
        });
    };
    readKeys();

    function delay(ms) {
        var cur_d = new Date();
        var cur_ticks = cur_d.getTime();
        var ms_passed = 0;
        while(ms_passed < ms) {
            var d = new Date();  // Possible memory leak?
            var ticks = d.getTime();
            ms_passed = ticks - cur_ticks;
            // d = null;  // Prevent memory leak?
        }
    }

    $scope.saveScreenshot = function() {
      alert('saving screenshot');
    };
  }])
  .controller('UserCtrl', ['$scope', function($scope) {
  	$scope.title = "User Controller Example";
  }])
  .controller('EventsCtrl', ['$scope', function($scope) {
    $scope.title = "Waiting Queue";

    function formattedDateTime() {
      var d = new Date();
      var hours = d.getHours();
      var pad = '00';
      var minutes = pad.substring(0, pad.length - (d.getMinutes()).toString().length) + d.getMinutes();
      var formattedDt = hours + ':' + minutes + ((hours >= 12) ? "PM" : "AM");
      return formattedDt;
    }

    
    $scope.events = [{text:'Garrett', icon: 'fa-warning', time: formattedDateTime(), color: randomColor() }];

    $scope.$on('requestControl', function(event, data) {
      
      var d = new Date();
      $scope.events.push({text: data, icon: 'fa-warning', time: formattedDateTime(), color: randomColor() });

    });

    function randomColor() {
      var colors = ["pink", "gold", "blue", "seagreen", "silver", "purple", "green", "orange"];
      var d = (new Date()).getMilliseconds() % colors.length;
      return colors[d];
    }

    $scope.refresh = function() {
      // retrieve more events
      alert('retrieving more events');

      // perhaps we want to use socket.io here or perhaps we want to query based on setInterval
    }
  }]);
