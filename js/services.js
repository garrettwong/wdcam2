'use strict';

var appServices = angular.module('myApp.services', []);

appServices.factory('notify', [function(win) {
    var getData = ["special data"];
    return {
      getGetData: function () { return getData; }
    };
  }]);

appServices.factory('graphService', [function() {
	return {
		getData: function () { return [1, 2, 3, 4, 5, 6] },
		getGraphActions: function () { return ["PCMark7", "PCMark8", "PCMark Vantage", "Enterprise IOMeter", "Jaws IOMeter", "IOMeter 4K"]; }
	};
}])