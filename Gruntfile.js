module.exports = function (grunt) {
    grunt.initConfig({
        watch: {
            files: "assets/scss/**/*.scss",
            tasks: ['compass'],
        },
        compass: {
            dist: {
                options: {
                    sassDir: 'assets/scss',
                    cssDir: 'assets/css',
                    outputStyle: 'compressed'
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'assets/css/*.css',
                        'index.html',
			'*.html',
                        'sortable-bar-chart.html'
                    ]
                },
                options: {
                    host: "192.168.0.1",
                    server: {
                        baseDir: "./"
                    }
                }
            }
        }
    });

    // load npm tasks
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');

    // create custom task-list
    grunt.registerTask('default', ["browserSync", "watch"]);
};
